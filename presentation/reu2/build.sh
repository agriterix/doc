#!/usr/bin/sh

# DATE_COVER=$(date "+%d %B %Y")

# SOURCE_FORMAT="markdown_strict\
# +pipe_tables\
# +backtick_code_blocks\
# +auto_identifiers\
# +strikeout\
# +yaml_metadata_block\
# +implicit_figures\
# +all_symbols_escapable\
# +link_attributes\
# +smart\
# +fenced_divs"

# pandoc -s --dpi=500 --slide-level 1 --toc --shift-heading-level=0 --columns=50 -H templates/preamble.tex --pdf-engine lualatex  -t beamer main.md -o main.pdf

# # --template templates/default_mod.latex 
# # -f "$SOURCE_FORMAT"
# # --listings
# # -M date="$DATE_COVER" -V classoption:aspectratio=1610 -V lang=fr-FR

pandoc -s main.md -t beamer -o main.pdf --pdf-engine=lualatex --slide-level=1
# pandoc -s main.md -t beamer -o main.pdf --pdf-engine=lualatex --slide-level=2

# pandoc -s main.md -t beamer -o main.pdf --pdf-engine=xelatex --from=gfm --slide-level=1
# pandoc -s main.md -t slidy -o main.html