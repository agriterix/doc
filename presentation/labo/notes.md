# Simulateur Agriterix

Je vais vous présenter le travail que j'ai effectué au cours de cette année sur le Simulateur Agriterix

# Projet

Ce projet est financé par l'i-site CAP 20-25 de Clermont, il s'inscrit dans le Challenge 1 sur les agrosystèmes durable en fournissant des outils d'aide à la décision.

# Propos du simulateur

Le propos du simulateur est de modéliser la transition agricole en s'intéressant à la production agricole, la démographie, les changements de pratiques, la consommation, les filières de transformation et de commercialisation.

L'intérêt est de poser une preuve de concept sur un territoire réduit pour étudier la faisabilité du projet et les données requises.

Le but est de mettre les scénrarios afteres 2050 à l'épreuve : impact des consommateurs en fonction des changements de pratique alimentaire ou des choix de filières de commercialisation ; l'impact des filières (création ou disparition d'entités de transformation ou de commercialisation); l'impact des rendements et l'impact des politiques publiques.

# Architecture

Voici le diagramme UML du simulateur actuel, c'est à dire l'ensemble des classes et leurs relations.

Je rentrerai dans les détails plus tard, mais en haut il y a l'instance du simulateur qui s'appuie à droite sur une contexte et des paramètres contenant un ensemble de à gauche on peut voir les structure d'exploitation (Farmer, Farm, Patch, Livestock) et de flux (Flow, Input) ainsi que la zone géographique (GeoArea) qui les lie.

# Dimension spatiale

Le simulateur comprend une dimension spatiale abstraite d'amplitude variable qui peut représenter différents niveaux spatiau (départements, PRA, etc.). 

Les entités (flux, parcelles, exploitations) ont une zone d'appartenance. L'aspect spatial peut donc se faire à posteriori, en reprojetant les parcelles et les zones par exemple. On peut gérer un ensemble de zones différentes en une instance.

# Exploitations

Une exploitation appartient à un exploitant, elles contient un ensemble de parcelles qui ont une culture, une pratique, une surface et une taille spécifique. Elle peut aussi disposer d'un cheptel qui contient un ensemble d'animaux.

# Flux

Un flux peut représente une structure de transformation, de consommation ou de distribution.

Il est défini par un ensemble de produit d'entrée avec une capacité maximum, des produits de sortie et un ratio de transformation

Par exemple une entreprise de panification est un flux que l'on peut représenter avec du blé et du lait en entrée avec des maximums de 200Kg et de 100L qui a pour produit de sortie du pain au lait, avec un ratio de 2Kg de Blé et 1L de Lait pour 2Kg de pain au lait.

# Dynamiques

Pour représenter les changements dans le simulateur on s'appuie sur un ensemple de dynamiques. La dynamique démographique qui a pour paramètres une matrice de transition de cultures, des paramètres d'ages et des probabilités de départs et de reprises. La dynamique de production agricole qui ne dépend que des de l'état courant du simulateur. Celle de marché alimentaire qui dépend de facteurs d'import et d'export. ainsi que la dynamique régissant les évolutions conjoncturelles qui est paramétrée par des scénarios exogènes prédéfinis.

Elles s'éxcécutent selon un pas de temps qui peut être variable (année, saison, mois, etc.).

# Dynamique de population

Pour la méthode simple on se base sur les répartitions d'âges de la MSA ainsi que les chiffres de départs en retraites.

# ABC Beta

Pour une méthode plus élaborée on peut se basée sur une loi beta paramétrée sur les courbes d'âge des chiffres de la MSA entre 2008 et 2018 ce qui nous permet d'obtenir une fonction de "vieillissement".

# Foncier

La dynamique de populatio implique aussi le foncier. Chaque départ en retraite d'un exploitant peut induire une reprise. Le simulateur contient une stratégie de reprises à 3 dimensions : le nombre de repreneurs, le fait qu'ils soient nouveaux arrivants ou existants et le maintien ou le changement de la nature de l'exploitation.

Les transitions du type de cultures se font sur via une matrice de transition pasée en paramètre qui détermine les probabilités de changements sur chaque culture. 

# Dynamique de production

La dynamique de production gèrent la production de produits dans chaque entité et donc le stock de ceux-ci. Pour les exploitations la production végétale se résume à la production de toutes les parcelles de l'exploitation en fonction des parèmtres de rendements de celles-ci. Pour la production animale, le cheptel inclut son propre ration de transformation (betes et alimentation/produit). Pour les flux la transformation est la transformation du stock de produit entrant en stock de produit sortant via le ratio de production.

# Dynamique de marché

La dynamique de marché se charge de l'allocation des stocks de production des entités vers les entrées de flux correspondantes en respectant les critères de quantités maximum absorbés. La précision pour l'allocation se fait via des tags (BIO, AOP) qui permettent de mieux orienter les produits. Pour le traffic hors zone on intéragit avec un flux global paramétrable qui importe et exporte vers toutes les zones.

# Dynamique de filières

Cette dynamique permet de représenter des évolutions conjoncturelles via des scénarios. Elle agit sur les flux, on peut préciser le type de flux, la ou les zones et l'horodatage de l'action pour agir sur le ratio ou la capacité d'absorption.

Cela permet de représenter une hausse de la consommation de légumineuses ou une baisse de productivité dans la filière lait par exemple.

# Données

Pour commencer à paramétrer le simulateur on utilise la base SIREN pour avoir un inventaire des entreprises agro-alimentaires dans le territoire. Le RPG, Cartobio et la statistique agricole annuelles permettes de paramétrer les parcelles avec les cutlures, les id d'exploitant, la localisation, la surface et la pratique. Les données démographiques de la MSA permettent d'obtenir une pyramide d'âge des exploitants agricoles.

Il faudra approfondir les recherches pour obtenir les rendements agricoles par pratiques, les transitions de cultures pour les reprises et les tendances de reprises pour mieux paramétrer la dynamique de population.

# Aspects techniques

On utilise la librairie XSTREAM de java pour sérialiser/déserialiser des états du simulateur et ainsi pouvoir générer un état du simulateur et en sortir un instantanné après X temps.

# Résultats

La modélisation de filères et et les dynamiques de reprises sont difficiles à modéliser en raison du manque de données. Mais en l'état le temps de calcul est plutôt rapide pour des paramètres calés sur le puy de dome.
