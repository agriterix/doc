---
title: Simulateur Agriterix
author: 
	- Loris Croce
	- Nicolas Dumoulin
	- Franck Jabot
institute: Laboratoire d'Ingénierie pour les Systèmes Complexes (LISC)
topic: Présentation labo
theme:   default # changer avec habillage
colortheme: dove # seagull
fonttheme: "structurebold"
innertheme: "circles"
titlegraphic: "img/inrae.png"
mainfont: "IBM Plex Sans"
monofont: "IBM Plex Mono"
fontsize: 10pt
aspectratio: 32
date: 06/07/2021
lang: fr-FR
section-titles: true
toc: false
slidy-url: https://www.w3.org/Talks/Tools/Slidy2
header-includes: # slide numbering and emoji support
  - \addtobeamertemplate{navigation symbols}{}{\usebeamerfont{footline}\usebeamercolor[fg]{footline}\hspace{1em}\insertframenumber}
  - \newcommand{\emoji}[1]{{\setmainfont{Noto Color Emoji}[Renderer=Harfbuzz]{#1}}}
---

# Projet

![](img/i-site.png){height=2cm}

Challenge 1
: *Agro-écosystèmes durables dans un contexte de changement global*

Objectif
: *Optimiser l'intégration des systèmes agricoles dans leur environnement et leur territoire*

Livrable 8
: *Des outils d'aide à la décision*


<!-- Objectif : mettre à disposition des modèles et outils d’aide à la décision pour des agro-écosystèmes durables à différentes échelles (de la parcelle au territoire) à destination des organisations professionnelles, chercheurs, ou collectivités locales -->

# Propos du simulateur

:::: columns

::: column

## Modéliser la transition agricole
  - \emoji{🐄🌾} Production
  - \emoji{🧑‍🌾👴} Démographie
  - \emoji{🌿🚜} Changement de pratiques
  - \emoji{🍕🥗} Consommation, filières de transformation et commercialisation

:::
>
::: column

## Preuve de concept
  - Faisabilité
  - Données requises
  - Résultats sur un territoire réduit

:::

::::

### Mettre les scénarios Afterres 2050 à l'épreuve

  - impact des consommateurs (changement de pratique alimentaire, ou choix des filières de commercialisation) ;
  - impact des filières (création ou disparition d'entités de transformation ou commercialisation) ;
  - impact des rendements ;
  - impact des politiques publiques.

# Architecture

![Diagramme UML](img/ModelPopFlux.png){height=80%}

---

## Dimension spatiale

- Zone géographique variable (PRA, département, …) d'appartenance des entités : flux, parcelles, exploitations ;
- Modèle non spatialement explicite, utilisation du spatial pour analyser les résultats ;
- Plusieurs zones géographiques par le simulateur.

![158901 parcelles RPG du Puy-de-Dôme (2019)](img/rpg.png){height=50%} 

---

## Exploitations

![Structure d'exploitation](img/farm.png){height=80%}

---

<!-- TODO d'abord l'exemple (dessin) -->
<!-- TODO aspect réseaux, pour la dynamique de marché... -->

### Exemple de flux

<!-- TODO qu'est-ce que c'est -->
Entreprise de panification :

- entrées : Blé (Max 200 Kg), Lait (Max 100L)
- sorties : Pain au lait
- ratio : 2Kg \emoji{🌾} + 1L \emoji{🥛} → 2Kg \emoji{🍞}

## Structure de flux

Modéliser les structures de transformation et de distribution :

- Entreprises agro-alimentaires, consommateurs etc.
- Produits d'entrées et de sorties
- Transformation avec ratio
- Capacité d'absorption maximum pour les produits d'entrées

<!-- TODO Graphique exemple transformation avec structure de flux -->

<!-- TODO Rajouter dessin et meilleur exemple sourcé -->

<!-- cas particulier flux conso → pas de sortie, et cas flux -->

# Dynamiques

| **Nom**        | **Paramètres** |
|:-----------|:-------------------------------|
| \emoji{👪} *Démographie*               | Matrices de transitions cultures, âges (pivot retraites, entrées et départs), probas de départs et de reprises | 
| \emoji{🌱} *Production agricole*        | (aucun)      |
| \emoji{🛒} *Marché alimentaire*         | Facteurs d'import et d'export | 
| \emoji{📈} *Évolutions conjoncturelles*  | Scénarios prédéfinis exogènes |

S’exécutent selon un pas de temps qui peut être variable (année, saison, mois, etc.).

# Dynamique de Population

<!-- mécanique démographique simple puis basé sur abc : les décrire -->
<!-- courbes d'ages -->
<!--  -->

## Mécaniques démographique

### Méthode simple 

Age pivot, probabilité de départ à la retraite.

![Installations, arrêt d'exploitation et taux de renouvèlement par type de production en Auvergne-Rhône-Alpes sur la période 2010-2018](img/donnees-msa.png){height=50%}

---

### Méthode basée sur abc : loi beta

 <!-- (Métamodèle) → fonction, années et données sources --> <!-- Mettre la fonction statistique et la décrire (loi beta tunée) -->


<!-- Here we define our model that computes the new numbers of individuals per age class at a year from the data from the precedent year.

We use a beta distribution for modeling the installation of new individuals and a linear factor for the retirement.
 -->

:::: columns

::: {.column width=60%}

- $N_{i}(y)$ : nombre d'individus pour classe $i$ à l'année $y$ ;
- $A$ : l'amplitude de la loi beta ; 
- $drop$ le coefficient de deformation de la loi beta ;
- $\alpha$ et $\beta$ les paramètres de forme ;
- $R_{t}$ l'age pivôt ;
- $R_{f}$ le facteur de départ;
- $\mu$ la moyenne ;
- $v = \alpha + \beta$

![](img/betaeq1.png)
![](img/betaeq2.png)
![](img/betaeq3.png)

:::

::: {.column width=40%}

$$
\left\{\begin{array}{lr}
\alpha = \mu * v\\
\beta = (1 - \mu) * v
\end{array}\right\}
$$

![Courbe d'âge calibrée par abc (MSA 2008--2018)](img/abc.png)

:::

::::
<!-- références sur abc -->


---

## Foncier

Stratégie de reprises à 3 dimensions : 

- 1 repreneur / repreneurs multiples
- Repreneur(s) existants / arrivants
- Maintien / transition du type de culture

→ Combinaison des 3 facteurs


## Transitions sur les cultures

<!-- Possibilité sur le type de pratique aussi -->

<!-- : Exemple de matrice de transition 

|        | Blé | Seigle | Orge |
|:------:|:---:|:------:|:----:|
| Blé    | 0.5 | 0.3    | 0.2  |
| Orge   | 0.1 | 0.4    | 0.5  |
| Seigle | 0.1 | 0.6    | 0.3  |
 -->

Matrices de transition → probabilités de changements de type de culture et de type de pratique à chaque reprise.

# Dynamique de Production

<!-- intro plus détail -->

<!-- ajouter un schema pour chaque entité -->

Objet *produit*, entités productives avec stock de produits.

![Exemple de filière](img/filieres.png){height=70%}

<!-- ## Exploitations 

- $\forall$parcelles : rendement(surface, pratique, produit) → production végétale
- cheptel : production animale avec ratio, exemple : 2\emoji{🐄} + 10\emoji{🌿} = 5\emoji{🥛} alimentation animale interne.

## Flux

Transformation avec ratio -->

# Dynamique de Marché

Graphe orienté de flux

:::: columns

::: column

- *Tag* des produits, exemple : Bio, AOP etc.
- Allocation sous contraintes du stock des exploitations/flux vers les entrées des flux de la zone. 
- Flux global pour représenter l'import/export en dehors de la zone d'étude.

:::

::: column

![Graphe de filière](img/FlowGraph.png)

:::

::::

<!-- tag : expl[bio] → flux[bio] → conso[bio] etc. -->
<!-- satisfaction des contraintes les flux absorbent le max des leurs entrées etc. -->
<!-- "Flux global" paramétrable pour chaque zone permet de gérer les import/exports hors de la zone -->
<!-- → ci dessus en schema -->

# Dynamique de Filières

Évolutions conjoncturelles → scénario

Selon le(s) type(s) de flux, la/les zone(s) et l'horodatage : 

- Baisse/hausse de productivité (ratio)
- Ajustement de la capacité d'absorption

### Exemples

- Hausse de la consommation de légumineuses.
- Baisse de la productivité dans la filière lait

# Données

- Utilisées : 

  Base SIREN
  : Initialisation de flux <!-- décrire -->

  RPG, CartoBio, SAA <!-- Agreste --> 
  : Parcelles(type de culture, exploitant, localisation, surface, pratique)

  Données démographiques MSA
  : Pyramide d'âge des exploitants

- Recherchées :
  - Rendements agricoles par pratique
  - Transitions de cultures (reprise)
  - Tendances de reprises d'exploitations

# Aspects techniques 

`XStream` 
: sérialisation/dé-serialisation d'états du simulateur.

\tiny

:::: columns

::: column

```xml
<Simulator>
  <geoAreas>
    <GeoArea label="minimal">
      <Flow>
        <inputs>
          <Input maximum="100.0">
            <product label="Ble" unit="Kg">
              <tags>
                <Tag>B</Tag>
                <Tag>D</Tag>
              </tags>
              <yield>1.0</yield>
            </product>
          </Input>
        </inputs>
        <quantities>
          <stocks/>
        </quantities>
        <outputs>
          <Product label="Pain" unit="Kg">
            <tags/>
            <yield>1.0</yield>
          </Product>
        </outputs>
        <ratio>
          <required>

```

:::

::: column

```xml
            <entry>
              <Product reference="../../../../inputs[1]/Input[1]/product[1]"/>
              <float>1.0</float>
            </entry>
          </required>
          <produced>
            <entry>
              <Product reference="../../../../outputs[1]/Product[1]"/>
              <float>1.0</float>
            </entry>
          </produced>
        </ratio>
        <id>Boulangerie</id>
      </Flow>
      <!-- [...] -->
  </dynamicsList>
  <timestamp>2021-07-05T17:15:29.334592</timestamp>
  <parameters>
    <rngSeedIndex>0</rngSeedIndex>
    <dynamics reference="../../dynamicsList[1]"/>
  </parameters>
</Simulator>
```

:::

::::

\normalsize

<!-- Cout de la simulation en fonction des données -->

<!-- expliquer le concept d'état du simulateur (en xml) avec le générateur qui produit un état -->

 <!-- Remettre l'uml et en montrant comment ça marche et ce que ça sort -->

# Résultats préliminaires

- Difficultés :
  - Modélisation du graphe de filières.
  - Dynamique démographique qui complique la modélisation.
- Temps de calcul rapide (~ 1min) sans optimisations particulières pour une simulation de 50 ans sur le territoire du Puy-de-Dôme (5000 exploitants et 160000 parcelles).

## Liens

- [`https://forgemia.inra.fr/agriterix`](https://forgemia.inra.fr/agriterix)
