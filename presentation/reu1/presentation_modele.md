---
title: Agriterix Simulator
author: 
	- Nicolas Dumoulin
	- Loris Croce
	- Franck Jabot
theme: Copenhagen
date: 19 Janvier 2020
# logo: '`logo_inrae.png`{=latex}'
aspectratio: 141 # 32
institute: UR LISC -- INRAE
lang: fr-FR
fontsize: 10pt
header-includes:
    - \usepackage{FiraSans}
    - \usepackage{FiraMono}
    - \usepackage{array}
# Générer les diapos : pandoc -t beamer presentation_modele.md -o presentation_modele.pdf
---

# Propos du modèle

<!-- - Intégrer les dynamiques de production agricole, de marché alimentaire, environnementales,
 du foncier et dynamique de population sur un territoire donné. -->

## Intégrer des dynamiques agro-alimentaires sur un ensemble de territoires.

- Étant données des parcelles et des unités de commercialisation dans des entités géographiques ;
- Simuler les dynamiques :
  - Du foncier et occupation du sol ;
  - Des exploitant-e-s ;
  - Production Agricole ;
  - Du marché alimentaire ;
  - Environnementale.

# Dynamique du foncier

<!-- - On a choisi de modéliser les parcelles comme des entités sans représentation spatiale.
- On aurait pu considérer les surfaces agrégées sur des zones géographiques (PRA par exemple)
- Une parcelle en friche peut disparaître suite à un scénario d'urbanisation (processus exogène)
- Une parcelle libérée par le départ d'un-e exploitant-e peut être reprise -->

- Pas de représentation spatiale des parcelles ;
- Détail de parcelles plutôt que des surfaces agrégées (e.g. PRA).

## Modèle minimal
- Une parcelle en friche peut disparaître suite à un scénario d'urbanisation (processus exogène) ;
- Une parcelle libérée par le départ d'un-e exploitant-e peut être reprise.
- Établissement de stratégies de reprises paramétrables selon le nombre d'exploitant-es reprenant, le modification des cultures lors des reprises. Exemple de critères :
  - économiques (e.g. poids d'une exploitation existante...) ;
  - démographiques (age des repreneur/ses) ;
  - culture (similarité des cultures et des pratiques).

# Dynamique des exploitants

## Modèle minimal
- À partir des données MSA :
  - modèle statistique des départs d'exploitant-e (retraite ou autre) ;
  - modèle statistique des installations.

![&nbsp;](fig/donnees-msa.png){ height=70% }

# Dynamique de production agricole

<!-- schema et description -->

<!-- Rendements moyens / hectare / production -->

Calculer un rendement moyen (au niveau de la parcelle) selon plusieurs facteurs :

## Minimal

- surface ;
- culture ;
- pratiques.

## Possible

- environnemental (météo, hydrologie, \dots) ;
- techniques (mécanisation, \dots) ;
- économiques.

# Dynamique de marché

- Modéliser les flux de matières produites depuis les exploitations, en passant par les filières de transformation jusqu'aux filières de commercialisation ;
- Réalisation un graphe d'entités commerciales recevant des flux de production et en produisant d'autres ;

![Exemple](../schema/ExempleModelFlux.png){ height=60% }

# Entités et variables d'état - UML

![](../schema/ModelPopFlux.png)

# Objectifs

- Étudier les conditions d'atteignabilité du Scenario Afterres 2050 :
  - impact des consommateurs (changement de pratique alimentaire, ou choix des filières de commercialisation) ;
  - impact des filières (création ou disparition d'entités de transformation ou commercialisation) ;
  - impact des rendements ;
  - impact des politiques publiques.

## Données recherchées

- rendement moyen à l'hectar par type de production ;
- graphe des filières (base SIREN, 3753 entreprises dans le 63 pour la division 10 "industrie alimentaires") ;
- transmission des exploitations agricoles.

## Calendrier

- mars : dynamique de production et exploitants
- mai : dynamique de filières (sur un territoire donné).
- juillet : études de scénarios

# Liens

- [`https://forgemia.inra.fr/agriterix`](https://forgemia.inra.fr/agriterix)
