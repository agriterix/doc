---
author:
- Loris [Croce]{.smallcaps}
geometry:
- a4paper
- scale=0.75
meta: pandoc -N -s main.md -o main.pdf --pdf-engine=lualatex && pandoc
  -N -s main.md -o main.md
title: |
  [Agriterix -- Documentation (WIP)]{.smallcaps}
---

# Introduction

![UML diagram of the
simulator.](../schema/ModelPopFlux.png){width="600px"}

The objective of the [Agriterix]{.smallcaps} simulator is to observe the
economic, environmental and societal impacts of changes in agricultural
consumption and production practices using a two-pronged approach:

-   The first one focuses on *individuals* for a land and demographic
    approach.
-   The second one focuses on *flows* to represent economic exchanges
    and transformation chains.

# Inputs and outputs files

A simulation can be created "from scratch" by using a generator with
inputs data sources. Otherwise a simulation instance can be loaded
(i.e. de-serialized) from an `.xml` file serialized by the XStream
library[^1].

A simulation relies on configuration files located in `data/in/config`
that specify behaviour of the simulator dynamics. Simulations logs and
instance files are located in a folder named after the timestamp of the
simulation start in `data/out/`.

# Data structures

The ambition of this simulator is to be able to graft complex or
simplified dynamics to it according to the needs, it is why their
operation is outsourced.

## Farmers and farms

A farmer is defined by an age, necessary for population dynamics, and
owns a farm. This farm is located in a geographical area. The farm has
two sources of production:

-   The *livestock*, which is composed of a set of animals with a
    respective number of animals. It also has a ratio that allows to
    specify the prerequisites for animal production with internal or
    external inputs.

-   The *patches* that have a surface area, a product resulting from
    their cultivation and a type of farming practice.

## Flows

A flow represents a *transformation chain*, it is the interface between
a production that may come from farms or from another flow. It is
defined by a set of inputs that are "raw materials" with a maximum
arrival and a set of outputs that define the production of the flow. It
is also specified what allows conversion between the input products.
This structure also has a stock for the products of inputs and outputs
not sold out.

## Spatial dimensions

To estimate proximity effects, the simulator integrates an abstract
spatial dimension.

### Geographical Areas

A geographical area comprises a total surface area and refers to a set
of flows and patches. This makes it possible to represent a space in
which internal exchanges are considered "local".

### Spatialized elements

![Example of a processing line
graph.](../schema/ExempleModelFlux.png){#transfo width="5cm"}

The location of flows and farms makes it possible to represent a graph
of *agricultural processing chains*. (figure
[2](#transfo){reference-type="ref" reference="transfo"}).

# Dynamics

The simulator has a set of dynamics to define the different evolution of
the system elements. Each can be linked to a specific time step (month,
year, etc.). All are associated with a dedicated logger that produces a
`.csv` log file in the simulation directory (TODO explain it), examples
of log files are detailed below.

## Production

The production dynamic coordinates the plant and animal productions of
all the farms. At first all farms produces their plant-based products
then their animal based products which need the latter. Next comes all
flow production.

   **Timestep**   **Geoarea**    **Product**    **Amount**      **Label**
  -------------- ------------- --------------- ------------ -----------------
        0           Allier        Blé (Kg)         123       LocalProduction
       365          Cantal      Carottes (Kg)      456       LocalProduction
       ...            ...            ...           ...             ...

  : Log file example

## Market

The market dynamic allocates production following a *tag-based*
allocation: for all products of all production we try to assign at first
a flow input having all tags and next at least one tag matching. If
there is no tag involved the previous condition is ignored and the
product stock from the Farm/Flow is transferred regarding the Input's
maximum constraint.

   **Timestep**   **Geoarea**    **Product**    **Amount**   **Label**
  -------------- ------------- --------------- ------------ -----------
        0           Allier        Blé (Kg)         123         local
        0           Allier      Oranges (Kg)       321        import
       365          Cantal      Carottes (Kg)      456        export
       ...            ...            ...           ...          ...

  : Log file example

The dynamics can be configured with export and import factors for each
GeoArea via a config file :

   **Timestep**   **GeoArea**   **Import factor**   **Export factor**
  -------------- ------------- ------------------- -------------------
        0           Allier             0.3                 0.7
       365          Cantal             0.8                 0.2
       ...            ...              ...                 ...

  : Config file example

## Population

Here we define our model that computes the new numbers of individuals
per age class at a year from the data from the precedent year.

We use a beta distribution for modeling the installation of new
individuals and a linear factor for the retirement.

Let be:

-   $N_{i}(y)$ the number of individual for the class $i$ for the year
    $y$
-   $A$ the amplitude of the beta distribution
-   $drop$ the coefficient for rescaling the shape of the beta
    distribution
-   $\alpha$ and $\beta$ the shape parameters of the beta distribution
-   $R_{t}$ the retirement threshold, i.e. the age when the retirement
    function starts
-   $R_{f}$ the retirement factor

We define the shifted number of individuals for the ageclass $i$ and the
year $y$ $N_{i}^{\star}(y)$, and the number of individuals for the
ageclass $i$ and the year $y+1$ $N_{i}(y+1)$

$$
N_{i}^{\star}(y) = \left\{\begin{array}{lr}
    0, & \text{for } i\leq 0\\
    N_{i-D}(y), & \text{for } i>0\\
    \end{array}\right\}
$$

$$
N_{i}(y+1) = \left\{\begin{array}{lr}
    N_{i}^{*}(y) * (1 + A * dbeta(\frac{i}{drop},\alpha,\beta)), & \text{for } i < R_{t}\\
    N_{i}^{*}(y) * (1 + A * dbeta(\frac{i}{drop},\alpha,\beta) - R_{f}), & \text{for } i\geq R_{t}\\
\end{array}\right\}
$$

From the binomial theorem[^2], we can develop for D years:

$$
N_{i}(y+D) = \left\{\begin{array}{lr}
    N_{i}^{*}(y) * \sum_{k=0}^{D} \tbinom{D}{k} A * dbeta(\frac{i}{drop},\alpha,\beta)^{k}, & \text{for } i < R_{t}\\
    N_{i}^{*}(y) * \sum_{k=0}^{D} \tbinom{D}{k} \sum_{j=0}^{k} \tbinom{k}{j} A * dbeta(\frac{i}{drop},\alpha,\beta))^{k-j}+ (-R_{t})^{j}, & \text{for } i\geq R_{t}\\
\end{array}\right\}
$$

### Beta parametrisation

As the $\alpha$ and $\beta$ parameters of the beta distribution is
correlated, we use the parametrisation with the mean $\mu$ and the
addition of both shape parameters $v = \alpha + \beta$ (Kruschke, John
K. (2011). Doing Bayesian data analysis: A tutorial with R and BUGS.
p. 83: Academic Press / Elsevier. ISBN 978-0123814852). We use the
relation:

$$
\left\{\begin{array}{lr}
\alpha = \mu * v\\
\beta = (1 - \mu) * v
\end{array}\right\}
$$

TODO add figures

### Cultural transition and land management

For cultural transition we use a 3 dimensions strategy:

-   Single / multiple retaker
-   Existing retaker or newcomer
-   Keeping or moving cultural type

In order to simulate a decision for the cultural transition we use a
transition matrix[^3].

            Wheat   Barley   Rye
  -------- ------- -------- ------
  Wheat      0.9     0.05    0.05
  Barley    0.05     0.9     0.05
  Rye       0.05     0.05    0.9

  : Transition matrix example

## Flow

The flow dynamic updates the attributes of the flows like their inputs
or ratio to build transformation or consumption transitions. It reads a
scenario file that specifies external changes like a productivity drop
or raise in a specific field and area at a certain time.

   **TimeStep**   **Product**   **Value**   **Category**   **Geoarea**
  -------------- ------------- ----------- -------------- -------------
        0             Blé          1.2         Inputs        Allier
       365          Fromage         2         Produced       Cantal
       730           Lait          1.3        Required       Allier
       ...            ...          ...          ...            ...

  : Config scenario example

# Hypothesis

[^1]: [`https://x-stream.github.io`](https://x-stream.github.io)

[^2]: [`https://en.wikipedia.org/wiki/Binomial_theorem`](https://en.wikipedia.org/wiki/Binomial_theorem)

[^3]: [`https://en.wikipedia.org/wiki/Stochastic_matrix`](https://en.wikipedia.org/wiki/Stochastic_matrix)
