# Agriterix documentation

This repository contains documentation related to the Agriterix simulator projects.
 - [abc-ages](https://forgemia.inra.fr/agriterix/abc-ages) : contains studies for the population dynamics.
 - [farm-generator](https://forgemia.inra.fr/agriterix/farm-generator) : contains preprocessing of farming data for generating initial state.
 - [import-export](https://forgemia.inra.fr/agriterix/import-export) : contains study about import/export.
 - [transitions](https://forgemia.inra.fr/agriterix/transitions) : contains tools for generating transitions matrix.
 - [simulator](https://forgemia.inra.fr/agriterix/simulator) : **the main simulator**
 - [dataprocess](https://forgemia.inra.fr/agriterix/dataprocess) : must be used for processing output of simulator.
 - [datamap](https://forgemia.inra.fr/agriterix/datamap) : tool for generating map and plots of the output processed data.

## UML Graph of the simulator

![](schema/ModelPopFlux.png)

## Documentation

The description of the model is located [here](documentation/main.pdf)

## Acquisition scenarios

A description of the different acquisitions scenarios is located [here](old/acquisition_scenarios.md).

## Presentation

Slides for 19/01/2020 (french) are available [here](presentation/presentation_modele.pdf)
