# Acquistion scenarios

Here is a list that covers all (or almost) acquistion scenarios ~~and their pseudo-code implementations associated~~. We consider that non-attributed patches are removed.

Choices between scenarios for each acquisition would 

## Single new identical

A new `Farmer` is created and the `Farm` corresponding to the previous `Farmer` is attached to the new one.

## Single new different

A new `Farmer` is created and some (or all) `Patch` of its `Farm` have their `CulturalType` and/or `CulturalPractice` modified. The number of `Patch` modified and their modifications are specified in the scenario regarding the initial attributes of each `Patch`.The `Farm` is finally attached to the new `Farmer`.

## Multiple new identical

*n* `Farmer` are created with each corresponing `Farm`. The `Patch` of `Farm` belonging to the previous `Farmer` are reassignated to the *n* new `Farm` created following a specified strategy :

- Using a *proportion* criteria to redistribute the `Patch`.
- Using a `CulturalType` criteria to redistribute the `Patch`.
- Using an *economic* criteria (to be implemented) to redistribute the `Patch`.

## Multiple new different

*n* `Farmer` are created with each corresponing `Farm`. The `Patch` of `Farm` belonging to the previous `Farmer` are reassignated to the *n* new `Farm` created following a specified strategy :

- Using a *proportion* criteria to redistribute the `Patch`.
- Using a `CulturalType` criteria to redistribute the `Patch`.
- Using an *economic* criteria (to be implemented) to redistribute the `Patch`.

For each new `Farmer` some (or all) `Patch` of its `Farm`. The number of `Patch` modified and their modifications are specified in the scenario regarding the initial attributes of each `Patch`.

## Single current identical

An existing `Farmer` adds all the `Patch` of the `Farm` of the previous `Farmer` to its own `Farm`.

## Single current different

Some (or all) `Patch` of the `Farm` of the previous `Farmer` have their `CulturalType` and/or `CulturalPractice` modified. The number of `Patch` modified and their modifications are specified in the scenario regarding the initial attributes of each `Patch`.

An existing `Farmer` adds all the `Patch` of the `Farm` of the previous `Farmer` to its own `Farm`.

## Multiple current identical

*n* existing `Farmer` are selected. The `Patch` of `Farm` belonging to the previous `Farmer` are reassignated to the *n* `Farm` selected following a specified strategy :

- Using a *proportion* criteria to redistribute the `Patch`.
- Using a `CulturalType` criteria to redistribute the `Patch`.
- Using a `CulturalPractice` criteria to redistribute the `Patch`.
- Using an *economic* criteria (to be implemented) to redistribute the `Patch`.

## Multiple current different

*n* existing `Farmer` are selected. The `Patch` of `Farm` belonging to the previous `Farmer` are reassignated to the *n* `Farm` selected following a specified strategy :

- Using a *proportion* criteria to redistribute the `Patch`.
- Using a `CulturalType` criteria to redistribute the `Patch`.
- Using a `CulturalPractice` criteria to redistribute the `Patch`.

For each selected `Farmer` some (or all) `Patch` of its `Farm`. The number of `Patch` modified and their modifications are specified in the scenario regarding the initial attributes of each `Patch` acquired.